function isPalindrome(text) {
    if ( text ) {
        console.log("===============================================");

        let regex = /[^a-z\u00C0-\u024F]/gi;
        text = text.toLowerCase().trim().replace(regex, "");
        regex = /[éèêë]/gi;
        text = text.replace(regex, "e");
        regex = /[ï]/gi;
        text = text.replace(regex, "i");
        regex = /[àâ]/gi;
        text = text.replace(regex, "a");
        regex = /[ùû]/gi;
        text = text.replace(regex, "u");
        regex = /[ôö]/gi;
        text = text.replace(regex, "o");
        console.log("Palindrome : " + text);
        console.log("Palindrome length : " + text.length);
        //Algostart
        for ( let i = 0; i <= text.length / 2; i++ ) {
            console.log(text[i] + "/" + text[text.length - (i + 1)]);
            if ( text[i] !== text[text.length - (i + 1)] ) return false;
        }
    }
    return true;
}

console.clear();
console.log("An na. ==> " + isPalindrome("An na."));
console.log("Bob ==> " + isPalindrome("Bob"));
console.log("malayalam ==> " + isPalindrome("malay alam.,"));
console.log("coloc ==> " + isPalindrome("coloc"));
console.log("sémèmes ==> " + isPalindrome("sémèmes"));
