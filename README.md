## Exercice 1.1.3 : Réaliser un vérificateur de palindrome (JavaScript)
Écrire une fonction qui établit pour une chaîne de caractères donnée s'il s'agit d'un palindrome ou non. Un palindrome est un mot ou une suite de mots qui se lisent de la même façon dans les deux sens comme "bob" ou "Anna". La fonction doit aussi vérifier les phrases en omettant les espaces.

Discutons en ensemble sur le forum :
[https://forum.alyra.fr/t/exercice-1-1-3-la-fonction-est-elle-un-palindrome/69](https://forum.alyra.fr/t/exercice-1-1-3-la-fonction-est-elle-un-palindrome/69)
